# imap_redirect.py

# developed from ideas from https://pymotw.com/3/imaplib/
# and https://docs.python.org/3/library/imaplib.html

# invoke with: python3 imap_redirect.py

import imaplib
import smtplib
import configparser
import os
import email
import email.parser
from pprint import pprint

def open_IMAP4_connection(config,section='source',verbose=False):
	# Connect to server with SSL
	hostname = config.get(section, 'hostname')
	if verbose:
		print('Connecting to', hostname)
	connection = imaplib.IMAP4_SSL(hostname)

	# Login to account
	username = config.get(section, 'username')
	password = config.get(section, 'password')
	if verbose:
		print('Logging in as', username)
	connection.login(username, password)
	return connection

def mail_redirect(imap,dest,verbose=False):
	typ, [msg_ids] = imap.uid('search',None,'ALL')
	if typ != 'OK':
		if verbose:
			print('no messages')
		return

	msg_list = ','.join(msg_ids.decode('utf-8').split(' '))
	if verbose:
		print('Matching messages:', msg_list)

	dest_server = dest.split('@')[1]

	with smtplib.SMTP(dest_server) as smtp:
		for msg_id in msg_ids.split():
			typ, msg_data = imap.uid('Fetch',msg_id,'(RFC822)')
			for response_part in msg_data:
				if isinstance(response_part, tuple):
					parser = email.parser.BytesFeedParser()
					parser.feed(response_part[1])
					msg = parser.close()
					if verbose:
						print('Message UID: ', msg_id)
						for header in ['Subject', 'To', 'From']:
							print('{:^8}: {}'.format(header, msg[header]))
					smtp.send_message(msg, None, dest)
					imap.uid('Store',msg_id,'+FLAGS',r'(\Seen \Deleted)')
		smtp.quit()

conffile = '';
# Read the config file
if conffile == '':
	conffile = os.path.expanduser('~/.mailaggrc')
config = configparser.ConfigParser()
config.read([conffile])

with open_IMAP4_connection (config, 'destination') as src:
	typ, data = src.select('to_bob_home')
	if typ == 'OK':
		mail_redirect(src, 'bob@2hgt.org', True)

	src.close()
	src.logout()
