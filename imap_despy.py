# imap_despy.py

# developed from ideas from https://pymotw.com/3/imaplib/
# and https://docs.python.org/3/library/imaplib.html

# invoke with: python3 imap_despy.py

import imaplib
# import smtplib
import configparser
import os
import email
import email.parser
import email.policy
import sys
from despy import *
from pprint import pprint

def open_IMAP4_connection (config, section='source', verbose=0):
	# Connect to server with SSL
	hostname = config.get (section, 'hostname')
	if verbose > 1:
		print('Connecting to', hostname)
	connection = imaplib.IMAP4_SSL (hostname)

	# Login to account
	username = config.get (section, 'username')
	password = config.get (section, 'password')
	if verbose > 1:
		print('Logging in as', username)
	connection.login (username, password)
	return connection

def process_part (part, verbose=0):
	if verbose > 3:
		print("Type of part is ", end = '')
		print(type(part))
	body = part.get_content()  # decode
	if verbose > 3:
		print ("Type of body is ", end = '')
		print (type(body))
		print (body)
	newbody = ""
	changed = False
	for line in body.split('\n'):
		newline = despy_line(line)
		if newline != line:
			changed = True
			if verbose > 2:
				print("CHANGED " + line)
				print()
				print("TO ---- " + newline)
				print()
		newbody += newline + "\r\n"
	if verbose > 3:
		print(newbody)
	if changed:
		return newbody
	return ""

def process_msg (response, verbose = 0):
	parser = email.parser.BytesFeedParser (policy=email.policy.default)
	parser.feed (response[1])
	msg = parser.close ()
	if verbose > 2:
		print(type(msg))
	if verbose > 4:
		print(msg)
	if verbose > 0:
		for header in ['Subject', 'To', 'From']:
			print('{:^8}: {}'.format(header, msg[header]))

	if msg.is_multipart():
		if verbose > 2:
			print("Multipart")
		for part in msg.walk():
			ctype = part.get_content_type()
			subtype = part.get_content_subtype()
			if verbose > 2:
				print(ctype)
				print(subtype)
			cdispo = str(part.get('Content-Disposition'))
			# skip any text/plain (txt) attachments
			if (ctype == 'text/plain' or ctype == 'text/html') and 'attachment' not in cdispo:
				new_body = process_part (part, verbose)
				if new_body != '':
					part.set_content(new_body, subtype)
		return msg

	# not multipart - i.e. plain text, no attachments, keeping fingers crossed
	else:
		if verbose > 2:
			print("Not multipart")
		subtype = msg.get_content_subtype()
		new_body = process_part(msg,verbose)  # decode
		if new_body !=  '':
			msg.set_content(new_body, subtype)
		return msg

def imap_move (src, dest, verbose = 0):
	typ, [msg_ids] = src.uid('search',None,'ALL')
	if typ != 'OK':
		if verbose > 0:
			print('no messages')
		return

	if verbose > 0:
		msg_list = ', '.join(msg_ids.decode('utf-8').split(' '))
		print('Matching messages: ', msg_list)

	for msg_id in msg_ids.split():
#		if msg_id > b'403700':
#			continue
		typ, msg_data = src.uid('Fetch',msg_id,'(RFC822)')
		for response_part in msg_data:
			if isinstance(response_part, tuple):
				if verbose > 1:
					print('Message UID: ', msg_id)
				if verbose > 3:
					print("Type of response_part is ", end = '')
					print(type(response_part))
				new_msg = process_msg(response_part, verbose)
				if verbose > 4:
					print(new_msg)
					
				dest.append('Inbox', '', None, str(new_msg).encode('utf-8'))
				src.uid('Store',msg_id,'+FLAGS',r'(\Seen \Deleted)')

def mail_redirect(imap,dest,verbose=0):
	typ, [msg_ids] = imap.uid('search',None,'ALL')
	if typ != 'OK':
		if verbose > 0:
			print('no messages')
		return

	if verbose > 0:
		msg_list = ', '.join(msg_ids.decode('utf-8').split(' '))
		print('Matching messages: ', msg_list)

	dest_server = dest.split('@')[1]

	with smtplib.SMTP (dest_server) as smtp:
		for msg_id in msg_ids.split():
#			if msg_id == b'61':
#				continue
			typ, msg_data = imap.uid('Fetch',msg_id,'(RFC822)')
			for response_part in msg_data:
				if isinstance(response_part, tuple):
					if verbose > 1:
						print('Message UID: ', msg_id)
					if verbose > 3:
						print("Type of response_part is ", end = '')
						print(type(response_part))
					new_msg = process_msg(response_part, verbose)
					if verbose > 4:
						print(new_msg)
					
					smtp.send_message(new_msg, None, dest)
#					imap.uid('Store',msg_id,'+FLAGS',r'(\Seen \Deleted)')
		smtp.quit()

conffile = '';
# Read the config file
if conffile == '':
	conffile = os.path.expanduser ('~/.mailaggrc')
config = configparser.ConfigParser ()
config.read ([conffile])

with open_IMAP4_connection (config, 'source') as src:
	with open_IMAP4_connection (config, 'destination') as dest:
		typ, data = src.select('Inbox')
		if typ == 'OK':
			imap_move (src, dest, 1)
			src.expunge()

	src.close()
	src.logout()
