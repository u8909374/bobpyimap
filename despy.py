# despy.py

import re
import urllib.parse

def safelinks(url):
	res = re.search(r"\?url=(.*?)\&", url)
	if res:
		return urllib.parse.unquote(res.group(1))
	return "BROKEN safe LINK"

def engage(url):
	res = re.search(r"\?target=(.*)", url)
	if res:
		return urllib.parse.unquote(res.group(1))
	return "BROKEN engage LINK"

def despy(url):
	res = re.search(r"https://([\w\-\.]+)/(.*)", url)
	if res:
		dns = res.group(1)
		if dns == "aus01.safelinks.protection.outlook.com":
			return despy(safelinks(res.group(2)))
		if dns == "engage.anu.edu.au":
			return despy(engage(res.group(2)))
	return url

def despy_line(instr):
	outstr = ""
	while len(instr) > 0:
		# (.*?) => don't be greedy, be lazy :-)
		res = re.search(r"(.*?)(https://[\w\-\.]+/[^\s()<>\"]*)(.*)", instr)
		if res:
			outstr += res.group(1) + despy(res.group(2))
			instr = res.group(3)
		else:
			outstr += instr
			break
	return outstr

